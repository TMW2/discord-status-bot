## Description
This bot can be used to inform you and your friends\
wich services are still running, can also highlight a specific role when one servive went down.

## Requirements
- Python 3.0.*
- discord<span>.</span>py
- psutil

## Install Req.
```
    sudo apt update
    sudo apt install python3
    pip3 install discord 
    pip3 install psutil
```

## Configuration
there is a 'help-block' on top of the .template file if you have any questions,\
feel free to open an issue, pm me or add me on Discord jak1#4141

you need to rename the file, just remove the `.template`

## Startup 
```
    ./discord_status_bot.py
```

## License
all Repository files are Licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)\
a copy can be found in the Repository [LICENSE](LICENSE)